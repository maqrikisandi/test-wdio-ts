import { MoladinExpectedConditionCommand } from './moladin.expected.condision.commands';
import { MobileElementCommand } from './moladin.element.commands';
import { MobileSelectCommand } from './moladin.select.commands';
import { MobileScollCommand } from './moladin.scroll.commands';
import { MoladinUtilityCommand } from './moladin.utility.commands';
import { MoladinSwipeCommand } from './moladin.swipe.commands';

class Commands {
    elementCondition = new MoladinExpectedConditionCommand();
    elementFinder = new MobileElementCommand();
    elementSelect = new MobileSelectCommand();
    elementScroll = new MobileScollCommand();
    elementSwipe = new MoladinSwipeCommand();
    utility = new MoladinUtilityCommand();
}

export default new Commands();