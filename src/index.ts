import Utils from "./helpers/utils";
import Commands from "./commands";

class MojaveEvoMobile {
    utils = Utils;
    commands = Commands;
}

export default new MojaveEvoMobile();