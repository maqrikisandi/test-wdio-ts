export class MoladinSwipeCommand {
    constructor() {
        this.byLocatorStrategySwipedElement = async (strategy, selector, direction = 'up') => {
            let mobileBy = 'xpath';
            let startCoordinate;
            let endCoordinate = { x: 0, y: 0 };
            const offset = -250;
            switch (strategy.toLowerCase()) {
                case 'xpath':
                    mobileBy = 'xpath';
                    break;
                case 'css':
                    mobileBy = 'css';
                    break;
                case 'uiautomator':
                    mobileBy = 'uiautomator';
                    break;
                default:
                    throw new Error('this kind of strategy not yet implemented');
            }
            const { ELEMENT } = await driver.findElement(mobileBy, selector);
            const { x: axisEl, y: ordinateEl, width: widthEl, height: heightEl, } = await driver.getElementRect(ELEMENT);
            switch (direction.toLowerCase()) {
                case 'down':
                    startCoordinate = { x: axisEl + widthEl / 2, y: ordinateEl + offset };
                    endCoordinate = { x: axisEl + widthEl / 2, y: ordinateEl + heightEl - offset };
                    break;
                case 'up':
                    startCoordinate = { x: axisEl + widthEl / 2, y: ordinateEl + heightEl - offset };
                    endCoordinate = { x: axisEl + widthEl / 2, y: ordinateEl + offset };
                    break;
                case 'left':
                    startCoordinate = { x: axisEl + widthEl - offset, y: ordinateEl + heightEl / 2 };
                    endCoordinate = { x: axisEl + offset, y: ordinateEl + heightEl / 2 };
                    break;
                case 'right':
                    startCoordinate = { x: axisEl + offset, y: ordinateEl + heightEl / 2 };
                    endCoordinate = { x: axisEl + widthEl - offset, y: ordinateEl + heightEl / 2 };
                    break;
                default:
                    throw new Error('this kind of strategy not yet implemented');
            }
            await driver.touchAction([
                Object.assign(startCoordinate, {
                    action: 'longPress',
                }),
                Object.assign(endCoordinate, {
                    action: 'moveTo',
                }),
                'release',
            ]);
        };
        this.screenSwipe = async (stringDirection) => {
            const { width, height } = await driver.getWindowSize();
            const startCoordinate = { x: width / 2, y: height / 2 };
            let endCoordinate = {};
            switch (stringDirection.toLowerCase()) {
                case 'down':
                    endCoordinate = { x: width / 2, y: height };
                    break;
                case 'up':
                    endCoordinate = { x: width / 2, y: startCoordinate.y - 550 };
                    break;
                case 'left':
                    endCoordinate = { x: 0, y: height / 2 };
                    break;
                case 'right':
                    endCoordinate = { x: width, y: height / 2 };
                    break;
                default:
                    throw new Error('This kind of direction not yet supported');
            }
            await driver.touchAction([
                Object.assign(startCoordinate, {
                    action: 'longPress',
                }),
                Object.assign(endCoordinate, {
                    action: 'moveTo',
                }),
                'release',
            ]);
        };
    }
}
