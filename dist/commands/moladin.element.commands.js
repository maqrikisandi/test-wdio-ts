/* global $, $$ */
export class MobileElementCommand {
    constructor() {
        this.finder = async (selector, strategy = 'CSS || XPATH') => {
            let prefixSelector = this.defaultSelector;
            if (strategy === 'UISelector') {
                prefixSelector = 'android=';
            }
            return $(`${prefixSelector}${selector}`);
        };
        this.finders = async (selector, strategy = 'CSS || XPATH') => {
            let prefixSelector = this.defaultSelector;
            if (strategy === 'UISelector') {
                prefixSelector = 'android=';
            }
            return $$(`${prefixSelector}${selector}`);
        };
        this.typeText = async (selector, text, strategy = 'CSS || XPATH') => {
            const element = await this.finder(selector, strategy);
            await element.clearValue();
            await element.addValue(text);
        };
        this.getText = async (selector, strategy = 'CSS || XPATH') => {
            const element = await this.finder(selector, strategy);
            const getText = await element.getText();
            return getText;
        };
        this.clickOn = async (selector, strategy = 'CSS || XPATH') => {
            const element = await this.finder(selector, strategy);
            await element.click();
        };
        /**
        * for changing value that having text %var with dynamicValue
        * i.e  '//span[normalize-space()="%var"]';
        */
        this.builderElement = async (element, dynamicValue) => {
            this.selector = element.replace('%var', dynamicValue);
            return this.selector;
        };
        this.defaultSelector = '';
        this.selector = '';
    }
}
