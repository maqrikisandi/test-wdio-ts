import { MobileElementCommand } from "./moladin.element.commands";
import { MoladinExpectedConditionCommand } from "./moladin.expected.condision.commands";
import { MoladinSwipeCommand } from "./moladin.swipe.commands";
export class MobileScollCommand {
    constructor() {
        this.byUISelectorScrollToElement = async (scrollableClassName, stringDirection, targetElementSelector) => {
            const scrollableLocator = `new UiSelector().className(${scrollableClassName}).scrollable(true)`;
            const elementID = await this._elementCommand.finder(scrollableLocator, 'UISelector');
            const options = {
                elementId: elementID,
                selector: targetElementSelector,
                strategy: '-android uiautomator',
                direction: stringDirection,
                maxSwipes: 30,
            };
            await driver.execute('mobile: scroll', options);
        };
        this.byScreenSwipeScrollToElement = async (locator, swipeDirection, strategy = 'CSS || XPATH') => {
            let counterSwipe = 0;
            let elementDisplayed = await this._moladinExpectCondition
                .isElementDisplayed(locator, strategy);
            while (!elementDisplayed && counterSwipe < 50) {
                await this._moladinSwipe.screenSwipe(swipeDirection);
                elementDisplayed = await this._moladinExpectCondition
                    .isElementDisplayed(locator, strategy);
                counterSwipe += 1;
            }
        };
        this.byDragActionScrollToElement = async (locator, swipeDirection, strategy = 'xpath') => {
            let counterSwipe = 0;
            let elementDisplayed = await this._moladinExpectCondition
                .isElementDisplayed(locator, strategy);
            while (!elementDisplayed && counterSwipe < 10) {
                await this._moladinSwipe
                    .byLocatorStrategySwipedElement(strategy, locator, swipeDirection);
                elementDisplayed = await this._moladinExpectCondition
                    .isElementDisplayed(locator, strategy);
                counterSwipe += 1;
            }
        };
        this._moladinExpectCondition = new MoladinExpectedConditionCommand();
        this._moladinSwipe = new MoladinSwipeCommand();
        this._elementCommand = new MobileElementCommand();
    }
}
