import { MobileElementCommand } from "./moladin.element.commands";
import { MoladinExpectedConditionCommand } from "./moladin.expected.condision.commands";
import { MoladinSwipeCommand } from "./moladin.swipe.commands";
export class MobileSelectCommand {
    constructor() {
        this._moladinExpectCondition = new MoladinExpectedConditionCommand();
        this._moladinSwipe = new MoladinSwipeCommand();
        this._elementCommand = new MobileElementCommand();
    }
    async byVisibleTextOnDropDown(listSelector, textTobeSelect, strategy = 'CSS || XPATH') {
        const listOption = await this._elementCommand.finders(listSelector, strategy);
        for (const option of listOption) {
            const stringMatcher = await option.getText();
            if (stringMatcher === textTobeSelect) {
                await option.click();
                break;
            }
        }
    }
    async byVisibleElementOnDropDown(listSelector, textTobeSelect, maxSwipe = 50, strategy = 'CSS || XPATH') {
        const targetSelector = `//*[@text="${textTobeSelect}"]`;
        const targetListSelector = await this._elementCommand.finders(listSelector, strategy);
        const indexScrollable = Math.floor(targetListSelector.length / 2);
        const scrollableSelector = `(${listSelector})[${indexScrollable}]`;
        let swipeAttempt = 0;
        let elementDisplay = await this._moladinExpectCondition
            .isElementDisplayed(targetSelector, strategy);
        while (!elementDisplay && swipeAttempt < maxSwipe) {
            await this._moladinSwipe.byLocatorStrategySwipedElement('xpath', scrollableSelector, 'up');
            elementDisplay = await this._moladinExpectCondition
                .isElementDisplayed(targetSelector, strategy);
            swipeAttempt += 1;
        }
        try {
            await this._elementCommand.clickOn(targetSelector, strategy);
        }
        catch (error) {
            throw new Error('Element Could not Clickable due to still not displayed, please check your max swipe attempt');
        }
    }
}
