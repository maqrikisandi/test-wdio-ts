import { MobileElementCommand } from "./moladin.element.commands";
export class MoladinExpectedConditionCommand {
    constructor() {
        this.isElementDisplayed = async (selector, strategy = 'CSS || XPATH') => {
            const element = await this._elementCommand.finder(selector, strategy);
            const displayed = await element.isDisplayed();
            return displayed;
        };
        this.isElementNotDisplayed = async (selector, strategy = 'CSS || XPATH') => {
            const element = await this._elementCommand.finder(selector, strategy);
            const displayed = await element.isDisplayed();
            return !(displayed);
        };
        this.waitUntilElementDisplayed = async (selector, timeoutDuration = 10000, strategy = 'CSS || XPATH') => {
            const element = await this._elementCommand.finder(selector, strategy);
            await element.waitUntil(async () => (await this.isElementDisplayed(selector, strategy)) === true, { timeout: timeoutDuration, timeoutMsg: 'Element Not Found' });
        };
        this.waitUntilElementNotDisplayed = async (selector, timeoutDuration = 10000, strategy = 'CSS || XPATH') => {
            const element = await this._elementCommand.finder(selector, strategy);
            await element.waitUntil(async () => (await this.isElementNotDisplayed(selector, strategy)) === true, { timeout: timeoutDuration, timeoutMsg: 'Element Still Found' });
        };
        this._elementCommand = new MobileElementCommand();
    }
}
