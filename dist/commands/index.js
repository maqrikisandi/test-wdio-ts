import { MoladinExpectedConditionCommand } from './moladin.expected.condision.commands';
import { MobileElementCommand } from './moladin.element.commands';
import { MobileSelectCommand } from './moladin.select.commands';
import { MobileScollCommand } from './moladin.scroll.commands';
import { MoladinUtilityCommand } from './moladin.utility.commands';
import { MoladinSwipeCommand } from './moladin.swipe.commands';
class Commands {
    constructor() {
        this.elementCondition = new MoladinExpectedConditionCommand();
        this.elementFinder = new MobileElementCommand();
        this.elementSelect = new MobileSelectCommand();
        this.elementScroll = new MobileScollCommand();
        this.elementSwipe = new MoladinSwipeCommand();
        this.utility = new MoladinUtilityCommand();
    }
}
export default new Commands();
