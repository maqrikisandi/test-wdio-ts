/* global driver */
export class MoladinUtilityCommand {
    async delay(timeout) {
        await driver.pause(timeout);
    }
    async sendKeysEvent(keycode) {
        await driver.sendKeyEvent(keycode);
    }
    async backPage() {
        await driver.back();
    }
    async sessionReload() {
        const session = await driver.getSession();
        const isEmpty = Object.keys(session).length === 0;
        if (!isEmpty)
            await driver.reloadSession();
    }
    async relaunchApps() {
        await driver.startActivity('com.moladin.moladinagent.debug', 'com.moladin.moladinagent.module.splash.SplashActivity');
    }
    async startActivity(appPackage, appActivity) {
        await driver.startActivity(appPackage, appActivity);
    }
    async acceptAlert(buttonTextOrLabel) {
        const options = buttonTextOrLabel ? {
            buttonLabel: buttonTextOrLabel,
        } : {};
        await driver.execute('mobile: acceptAlert', options);
    }
    async dismissAlert(buttonTextOrLabel) {
        const options = buttonTextOrLabel ? {
            buttonLabel: buttonTextOrLabel,
        } : {};
        await driver.execute('mobile: dismissAlert', options);
    }
    async pushFileToDevice(pathFile, base64ImageData) {
        await driver.pushFile(pathFile, base64ImageData);
    }
    async deleteFileFromDevice(remotePathFile) {
        const options = remotePathFile ? {
            remotePath: remotePathFile,
        } : {};
        await driver.execute('mobile: deleteFile', options);
    }
    async openDeepLink(urlDeeplink) {
        const options = {
            url: urlDeeplink,
            package: 'com.moladin.moladinagent.debug',
        };
        await driver.execute('mobile: deepLink', options);
    }
    async executeADBShell(commandInput) {
        const options = {
            command: commandInput,
        };
        await driver.execute('mobile: shell', options);
    }
}
