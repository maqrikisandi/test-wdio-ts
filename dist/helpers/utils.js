class Utils {
    constructor() {
        /*
            This function to generate PhoneNumber without zero padding at first
        */
        this.fakerPhoneGenerator = async () => {
            const prefixProvider = '712';
            const timestampInSecond = Math.round(new Date().getTime() / 1000);
            return `${prefixProvider}${timestampInSecond}`;
        };
        /*
            This function to generate Nomor Induk Kependudukan using Dukcapil Valid Format
        */
        this.fakerNIKGenerator = async (prefix6Digit = '321503') => {
            const today = new Date();
            const yy = today.getFullYear().toString().substring(2);
            const mm = today.getMonth() + 1; // Months start at 0!
            const dd = today.getDate();
            let _dd = '';
            let _mm = '';
            if (dd < 10)
                _dd = `0${dd}`;
            if (mm < 10)
                _mm = `0${mm}`;
            const dateOfBirth = _dd + _mm + yy;
            const runningNoNIK = Math.floor(Math.random() * 2001) + 1001;
            return `${prefix6Digit}${dateOfBirth}${runningNoNIK}`;
        };
        this.randomAlphabetPicker = async () => {
            const alphabetArr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
            return alphabetArr[Math.floor(Math.random() * alphabetArr.length)];
        };
        /*
            This function to generate NomorPolisi randomize
        */
        this.fakerPoliceNoGenerator = async () => {
            let arrCharPoliceNo = [];
            // loop 3 times to collect char for construct policeNo
            for (let index = 0; index < 3; index += 1) {
                arrCharPoliceNo.push(await this.randomAlphabetPicker());
            }
            const prefixPoliceNo = arrCharPoliceNo[0];
            const suffixPoliceNo = `${arrCharPoliceNo[1]}${arrCharPoliceNo[2]}`;
            const runningNo = Math.floor(Math.random() * 8998) + 1001;
            return `${prefixPoliceNo}${runningNo}${suffixPoliceNo}`;
        };
        /*
            This function to normalize format Rp XXX.XXX.XXX into XXXXXXXXX
        */
        this.priceToNumber = (stringPriceRp) => {
            const normalizeText = stringPriceRp.replace(/[^0-9]/g, '');
            return normalizeText;
        };
        this.numberToPrice = (stringNumberRp) => {
            const normalizePrice = stringNumberRp.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            return normalizePrice;
        };
        this.sendReportPortalLog = async () => {
            // ReportingApi.log('INFO');
        };
    }
}
export default new Utils();
