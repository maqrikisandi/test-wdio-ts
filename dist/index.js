import Utils from "./helpers/utils";
import Commands from "./commands";
class MojaveEvoMobile {
    constructor() {
        this.utils = Utils;
        this.commands = Commands;
    }
}
export default new MojaveEvoMobile();
